Framework Files exchange platform
=================================

**For more information, see the [wiki](https://gitlab.com/timtizio/framework-files-exchange/wikis/home)**

The solution will create files exchange platform. Creating this kind of application must be done on the command line to make a basic version. An API will allow the users of this framework to come to graft different kinds of plugins. This plugins will be able to add some features or simply customize the default application behavior.

Here is the main development axis (See also [Architecture](https://gitlab.com/timtizio/framework-files-exchange/wikis/2.1.-Architecture)) :
  * [Users and Groups Manager](https://gitlab.com/timtizio/framework-files-exchange/wikis/2.1.-Architecture#211-users-and-groups)
  * [Files Manager](https://gitlab.com/timtizio/framework-files-exchange/wikis/2.1.-Architecture#212-files-manager)
  * [Plugin Engine](https://gitlab.com/timtizio/framework-files-exchange/wikis/2.1.-Architecture#213-plugin-engine)
  * [Front-end layout](https://gitlab.com/timtizio/framework-files-exchange/wikis/2.1.-Architecture#214-front-end)
  * [External API](https://gitlab.com/timtizio/framework-files-exchange/wikis/2.1.-Architecture#215-external-api)

An internal API allow the interactions with this actors and plugins.

The files storage must be done relocated on one or many servers. The solution will allow too some services to facilitate the files redundancy.

The authorization management done by the [Files Manager](https://gitlab.com/timtizio/framework-files-exchange/wikis/2.1.-Architecture#212-Files-Manager) according to the user and group who wants to access the file. It is managed like "UNIX". One file can be have different rights for the application admin, the owner (user and group) and others.
Custom authorizations can be added in the future by some plugins.

The front-end layout will be done with theme. This themes respect a convention in order to change it easily. A default theme will be proposed in the basic version.

The plugins allow to add some feature like shared diary, news, "to do list", newsletter system, etc.    


Technical
---------

This framework will be developed in PHP. He will based on the Lumen micro-framework to facilitate the database interaction, URL routing, API creation, etc.

To the front-end part, we will be able to use a JS framework like [React.JS](https://reactjs.org/), [Vue.JS](https://vuejs.org/) or [Angular](https://angular.io/)

**For more information, see the [wiki](https://gitlab.com/timtizio/framework-files-exchange/wikis/home)**
