  * Groups
    * onGroupAdding
    * onGroupEditing
    * onGroupRemoving
    * onGroupChangingRoles
  * Users
    * onUserRegistering
    * onUserEditing
    * onUserRemoving
    * onUserChangingRoles
    * onUserGroupAssignating
    * onUserGroupUnDeassignating
  * File system
    * onNodeAdding
    * onNodeRemoving
    * onNodeMoving
    * onChangedNodePermission
    * onChangedNodeOwner
    * onChangedNodePhysicalLocation
    * onServerAdding
    * onServerRemoving
    * onServerEditing
  * Plugin
    * onPluginActivation
    * onPluginDeactivation
    * onPluginInstallation
    * onPluginDeinstallation
  * Personal User Data
    * onPersonalDataEditing

